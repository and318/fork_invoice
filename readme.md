# Fork Invoice
Hola,

Gracias por la oportunidad de trabajo en Fork.

Les cuento de qué forma aborde el ejercicio dividida en 2 partes:

- Usabilidad
- Técnico

### Usabilidad

Lo más importante para realizar el diseño fue tratar de destacar y mostrar de forma clara lo más importante para el cliente, en este caso los productos comprados, valor, descuentos y total. Como cliente me gusta tener control en lo que gasto y por lo cual agregué el detalle de cada ítem y su cantidad.
A las instrucciones de retiro sumé un enlace a la ubicación de la tienda, lo cual me entrega la posibilidad de abrir directamente Google maps o Waze desde mi teléfono y no tener que estar buscando de forma manual.


### Técnico

Pensé en usar un framework para el código, pero no se podrían validar mis conocimientos de manera clara.
En el repositorio existen 2 archivos HTML (invoice e invoice-inline), en el primero el CSS está en el HEAD de la estructura y en el segundo está inline en el código, pues algunos clientes de correo no leen bien el css. (Los media query quedaron en el Head)

Revise el sitio de fork para tratar de mantener un poco la línea gráfica, pero hice algunas variaciones para mantenerlo más simple.
